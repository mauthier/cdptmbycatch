# cdptmbycatch

Supplementary information on Rouby et al. (Frontiers in Marine Science, 2022)

Paper available either at publisher's website: https://www.frontiersin.org/articles/10.3389/fmars.2021.795942/full

or on archimer : https://archimer.ifremer.fr/doc/00744/85628/
    
1. includes a shiny app to visualize all results (per ICES divisions) in folder 'shiny'
To use the app, please open the script 'runApp.R' and run lines 1 to 5. 
The following libraries are needed for the app: *tidyverse*, *shinydashboard*, *shiny*, *sf*, *ggplot2*, *leaflet*, *DT*, *htmltools* and *shinyWidgets*;
2. includes **Stan** code in folder 'stanmodel': the 6 models described in the associated paper are included;
3. includes a text file named '20211008_ModelBasedEstimates_CommonDolphins.txt' with all inferences in a long table format in folder 'data'. 
These estimates are the same as the one reported in the associated paper;
4. includes a synthetic dataset to run the code. 
These data were generated from the posterior predictive distribution of model $$\mathcal{M}_6$$.
