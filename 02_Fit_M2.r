##--------------------------------------------------------------------------------------------------------
## Last update : 2021-09-28
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("rstan"),
       library, character.only = TRUE
       )

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

M2 <- stan_model(file = paste("stanmodel", "M2.stan", sep = "/"),
                 model_name = "Saisine"
                 )

load("data/PTM_synthetic_standata.RData")

ptm_model <- fitmodel(standata = ptm_standata,
                      stanmodel = M2,
                      pars = c("intercept", "alpha", "delta", "epsilon", "w", "sigma", "log_lik", "shape", "rho")
                      )

looic <- loo::loo(loo::extract_log_lik(ptm_model), save_psis = TRUE)

save(list = c("ptm_model", "looic"),
     file = "PTM_stanmodel_M2.RData"
     )
