matern_cov <- function(d, sill = 1, range = 1) {
  sill * sill * (1.0 + abs(d) * sqrt(3.0) / range) * exp(-abs(d) * sqrt(3.0) / range)
}

# Intra-year variation
week <- 1:53
R <- matrix(0, nrow = 53, ncol = 53)
# R is the correlation matrix between weeks, chosen so that correlation is 0.05 after 4 weeks

R[1, ] <- 1:53 - 1
R[, 1] <- R[1, ]

for(i in 2:(nrow(R) - 1)) {
  R[i, (i + 1):nrow(R)] <- week[1:length((i + 1):nrow(R))]
  R[(i + 1):nrow(R), i] <- R[i, (i + 1):nrow(R)]
}; rm(i)
R <- matern_cov(d = R, range = 1.5) # R is now a correlation matrix # Range d?finit pour avoir correlation de 4 semaines
rm(week)
# View(round(R, 2)) 
# plot(seq(0, 5, 0.1), matern_cov(d = seq(0, 5, 0.1), range = 2), type = 'l')

standata <- function(l) {
  list(n_op = nrow(l$op),
       n_week = 53,
       n_zone = 10,
       n_year = diff(range(l$op$year)) + 1,
       BYCATCH = l$op$event,
       DURATION = l$op$opDuration / 60,
       WEEK = l$op$week,
       ZONE = l$op$ices,
       YEAR = l$op$year - min(l$op$year) + 1,
       n_maree = nrow(l$maree), 
       OP = l$maree$n_op,
       DaS = l$maree$n_days,
       WEEK3 = l$maree$week,
       ZONE3 = l$maree$ices,
       YEAR3 = l$maree$year - min(l$maree$year) + 1,
       prior_scale_sigma = c(log(10) / 2, log(2) / 3, log(2) / 2),
       prior_scale_intercept = c(1.5, 5.0, 5.0),
       R = R
       )
}

fitmodel <- function(standata,
                     stanmodel,
                     n_chains = 4,
                     n_iter = 2000,
                     n_warm = 1000,
                     n_thin = 1,
                     pars = c("intercept", "alpha", "delta", "epsilon", "w", "sigma", "log_lik", "Omega"),
                     control_hmc = list(adapt_delta = 0.99, max_treedepth = 15)
                     ) {
  fit <- sampling(object = stanmodel,
                  data = standata,
                  pars = pars,
                  chains = n_chains,
                  iter = n_iter,
                  warmup = n_warm,
                  thin = n_thin,
                  control = control_hmc
                  )
  return(fit)
}

theta <- function(stanfit,
                  week,
                  year, 
                  response = c("risk", "op_duration", "op_nb"),
                  param = c("average", "deviation"), 
                  mode = TRUE
                  ) {
  n_iter <- nrow(rstan::extract(stanfit, 'intercept')$intercept)
  if(any(stanfit@model_pars == "new_vessel")) {
    new_vessel <- rstan::extract(stanfit, 'new_vessel')$new_vessel * ifelse(include_vessel, 1, 0)
  } else{
    new_vessel <- matrix(0, nrow = n_iter, ncol = 3)
  }
  # work with mode for gamma distribution?
  if(any(stanfit@model_pars == "shape") && mode) {
    m <- as.numeric(rstan::extract(stanfit, 'shape')$shape)
    new_vessel[, 2] <- new_vessel[, 2] + log(m - 1) - log(m)
  }
  n_cell <- stanfit@par_dims$alpha[1]
  # chose response variable
  idy <- switch(response,
                risk = 1,
                op_duration = 2,
                op_nb = 3
                )
  # chose what to report
  x <- switch (param,
    average = matrix(rep(rstan::extract(stanfit, 'epsilon')$epsilon[, idy, week], each = n_cell), byrow = TRUE, ncol = n_cell) +
      rstan::extract(stanfit, 'alpha')$alpha[, , idy] +
      matrix(rep(new_vessel[, idy], each = n_cell), byrow = TRUE, ncol = n_cell),
    deviation = matrix(rep(rstan::extract(stanfit, 'delta')$delta[, idy, year, week], each = n_cell), byrow = TRUE, ncol = n_cell) +
      rstan::extract(stanfit, 'alpha')$alpha[, , idy] +
      matrix(rep(new_vessel[, idy], each = n_cell), byrow = TRUE, ncol = n_cell)
  )
  # back-transform
  y <- switch(response,
              risk = plogis(x),
              op_duration = exp(x),
              op_nb = exp(x) / (1 - exp(-exp(x))) # zero-truncated poisson
              )
  return(y)
}

get_loo <- function(model) {
  load(paste("output/", model, ".RData", sep = ""))
  x <- loo::loo(loo::extract_log_lik(ptm_model))
  return(x)
}

lower <- function(x, alpha = 0.2) {
  x <- coda::as.mcmc(x)
  return(coda::HPDinterval(x, prob = 1 - alpha)[1])
}

upper <- function(x, alpha = 0.2) {
  x <- coda::as.mcmc(x)
  return(coda::HPDinterval(x, prob = 1 - alpha)[2])
}

theta <- function(stanfit,
                  week,
                  year, 
                  response = c("risk", "op_duration", "op_nb"),
                  param = c("average", "deviation"), 
                  mode = TRUE
                  ) {
  
  n_iter <- nrow(rstan::extract(stanfit, 'intercept')$intercept)
  if(any(stanfit@model_pars == "new_vessel")) {
    new_vessel <- rstan::extract(stanfit, 'new_vessel')$new_vessel * ifelse(include_vessel, 1, 0)
  } else{
    new_vessel <- matrix(0, nrow = n_iter, ncol = 3)
  }
  # work with mode for gamma distribution?
  if(any(stanfit@model_pars == "shape") && mode) {
    m <- as.numeric(rstan::extract(stanfit, 'shape')$shape)
    new_vessel[, 2] <- new_vessel[, 2] + log(m - 1) - log(m)
  }
  n_week <- stanfit@par_dims$epsilon[2]
  n_cell <- stanfit@par_dims$alpha[2]
  # chose response variable
  idy <- switch(response,
                risk = 1,
                op_duration = 2,
                op_nb = 3
                )
  # chose what to report
  if(year == 0) {
    x <- rstan::extract(stanfit, 'epsilon')$epsilon[, idy, ] +
      matrix(rep(new_vessel[, idy], each = n_week), byrow = TRUE, ncol = n_week)
  } else {
    x <- switch (param,
                 deviation = rstan::extract(stanfit, 'alpha')$alpha[, idy, , year, week] +
                   matrix(rep(new_vessel[, idy], each = n_cell), byrow = TRUE, ncol = n_cell),
                 average = matrix(rep(rstan::extract(stanfit, 'delta')$delta[, idy, year, week], each = n_cell), byrow = TRUE, ncol = n_cell) +
                   matrix(rep(new_vessel[, idy], each = n_cell), byrow = TRUE, ncol = n_cell)
                 )
  }
  # back-transform
  y <- switch(response,
              risk = plogis(x),
              op_duration = exp(x),
              op_nb = exp(x) / (1 - exp(-exp(x)))
              )
  return(y)
}
