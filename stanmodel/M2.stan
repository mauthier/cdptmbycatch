data {
	int<lower = 1> n_op;
	int<lower = 1> n_week;
	int<lower = 1> n_zone;
	int<lower = 1> n_year;
  int<lower = 0, upper = 1> BYCATCH[n_op];
  vector<lower = 0.0>[n_op] DURATION;
  int<lower = 1, upper = n_week> WEEK[n_op];
  int<lower = 1, upper = n_zone> ZONE[n_op];
  int<lower = 1, upper = n_year> YEAR[n_op];
  int<lower = 1> n_maree;
  int<lower = 1> OP[n_maree];
  int<lower = 1> DaS[n_maree];
  int<lower = 1, upper = n_week> WEEK3[n_maree];
  int<lower = 1, upper = n_zone> ZONE3[n_maree];
  int<lower = 1, upper = n_year> YEAR3[n_maree];
  corr_matrix[n_week] R; // correlation matrix for intra-year level variations
  vector<lower = 0.0>[3] prior_scale_sigma;
  vector<lower = 0.0>[3] prior_scale_intercept;
}

transformed data {
  int<lower = 0> shiftedOP[n_maree];
  for(i in 1:n_maree) {
    shiftedOP[i] = OP[i] - 1;
  }
}

parameters {
  vector[3] u_intercept;
  simplex[3] prop[3];
  vector<lower = 0.0>[3] sigma2;
  vector<lower = 0.0>[3] tau;
  vector[3] u_epsilon[n_week - 1];
  vector[n_zone] u_alpha1;
  vector[n_zone] u_alpha2;
  vector[n_zone] u_alpha3;
  vector[n_year] u_delta1;
  vector[n_year] u_delta2;
  vector[n_year] u_delta3;
  real<lower = 0.0> cv;
  cholesky_factor_corr[3] Omega[3];
}

transformed parameters {
  matrix[3, 3] CorMat[3];
  vector[3] rho[3];
  real shape = 1 / square(cv);
  vector[3] intercept;
  vector[3] sigma[3];
  vector[n_week] epsilon[3];
  vector[n_week] delta[3, n_year];
  vector[n_week] alpha[3, n_zone, n_year];
  vector[n_op] linpred[2];
  vector[n_maree] linpred3;
  // variance components
  sigma[1] = prior_scale_sigma[1] * sqrt(prop[1] * sigma2[1] / tau[1]); // 1-week, 2-zone, 3-year
  sigma[2] = prior_scale_sigma[2] * sqrt(prop[2] * sigma2[2] / tau[2]); // 1-week, 2-zone, 3-year
  sigma[3] = prior_scale_sigma[3] * sqrt(prop[3] * sigma2[3] / tau[3]); // 1-week, 2-zone, 3-year
  // intercepts
  intercept = prior_scale_intercept .* u_intercept;
  // random walk
  epsilon[1, 1] = intercept[1]; // logit scale
  epsilon[2, 1] = intercept[2]; // log scale
  epsilon[3, 1] = intercept[3]; // log scale
  for(t in 2:n_week) {
    epsilon[1, t] = epsilon[1, t - 1] + u_epsilon[t - 1, 1] * sigma[1, 1];
    epsilon[2, t] = epsilon[2, t - 1] + u_epsilon[t - 1, 2] * sigma[2, 1];
    epsilon[3, t] = epsilon[3, t - 1] + u_epsilon[t - 1, 3] * sigma[3, 1];
  }
  // correlated random effects: non centered parametrization
  // use cholesky factors Omega
  for(k in 1:n_year) {
    delta[1, k] = epsilon[1] + sigma[1, 3] * (Omega[3, 1, 1] * rep_vector(u_delta1[k], n_week));
    delta[2, k] = epsilon[2] + sigma[2, 3] * (Omega[3, 2, 1] * rep_vector(u_delta1[k], n_week) + Omega[3, 2, 2] * rep_vector(u_delta2[k], n_week));
    delta[3, k] = epsilon[3] + sigma[3, 3] * (Omega[3, 3, 1] * rep_vector(u_delta1[k], n_week) + Omega[3, 3, 2] * rep_vector(u_delta2[k], n_week) + Omega[3, 3, 3] * rep_vector(u_delta3[k], n_week));
    for(j in 1:n_zone) {
      alpha[1, j, k] = delta[1, k] + sigma[1, 2] * (Omega[2, 1, 1] * rep_vector(u_alpha1[j], n_week));
      alpha[2, j, k] = delta[2, k] + sigma[2, 2] * (Omega[2, 2, 1] * rep_vector(u_alpha1[j], n_week) + Omega[2, 2, 2] * rep_vector(u_alpha2[j], n_week));
      alpha[3, j, k] = delta[3, k] + sigma[3, 2] * (Omega[2, 3, 1] * rep_vector(u_alpha1[j], n_week) + Omega[2, 3, 2] * rep_vector(u_alpha2[j], n_week) + Omega[2, 3, 3] * rep_vector(u_alpha3[j], n_week));
    }
  }
  // linear predictor
  for(i in 1:n_op) {
    linpred[1, i] = alpha[1, ZONE[i], YEAR[i], WEEK[i]];
    linpred[2, i] = exp(alpha[2, ZONE[i], YEAR[i], WEEK[i]]);
  }
  for(i in 1:n_maree) {
    linpred3[i] = alpha[3, ZONE3[i], YEAR3[i], WEEK3[i]] + log(DaS[i]);
  }
  for(m in 1:3) {
    CorMat[m] = multiply_lower_tri_self_transpose(Omega[m]);
    rho[m, 1] = CorMat[m, 1, 2];
    rho[m, 2] = CorMat[m, 1, 3];
    rho[m, 3] = CorMat[m, 2, 3];
  }
}

model {
  for(m in 1:3) {
    Omega[m] ~ lkj_corr_cholesky(1); // LKJ prior on the correlation matrix
  }
  cv ~ gamma(2.0, 5.0);
  u_intercept ~ normal(0.0, 1.0);
  sigma2 ~ gamma(0.5, 1.0);
  tau ~ gamma(1.0, 1.0);
  for(t in 1:(n_week - 1)) {
    u_epsilon[t] ~ multi_normal_cholesky(rep_vector(0.0, 3), Omega[1]);
  }
  u_alpha1 ~ normal(0.0, 1.0);
  u_alpha2 ~ normal(0.0, 1.0);
  u_alpha3 ~ normal(0.0, 1.0);
  u_delta1 ~ normal(0.0, 1.0);
  u_delta2 ~ normal(0.0, 1.0);
  u_delta3 ~ normal(0.0, 1.0);
  target += bernoulli_logit_lpmf(BYCATCH| linpred[1]);
  for(i in 1:n_op) {
    target += gamma_lpdf(DURATION[i]| shape, shape / linpred[2, i]);
  }
  for(i in 1:n_maree) {
    target += poisson_log_lpmf(OP[i]| linpred3[i]) - log1m_exp(poisson_log_lpmf(0| linpred3[i]));
  }
}

generated quantities {
  vector[n_op + n_maree] log_lik;
  real w[n_week, n_year, n_zone];
  for(i in 1:n_op) {
    log_lik[i] = bernoulli_logit_lpmf(BYCATCH[i]| linpred[1, i]) + gamma_lpdf(DURATION[i]| shape, shape / linpred[2, i]);
  }
  for(i in 1:n_maree) {
    log_lik[n_op + i] = poisson_log_lpmf(OP[i]| linpred3[i]) - log1m_exp(poisson_log_lpmf(0| linpred3[i]));
  }
  for(t in 1:n_week) {
    for(k in 1:n_year) {
      for(j in 1:n_zone) {
        w[t, k, j] = (exp(alpha[3, j, k, t]) / (1 - exp(-exp(alpha[3, j, k, t])))) * inv_logit(alpha[1, j, k, t]);
      }
    }
  }
}
