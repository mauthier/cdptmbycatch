##--------------------------------------------------------------------------------------------------------
## SCRIPT : Inferences from M6
##
## Authors : Matthieu Authier
## Last update : 2021-10-01
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "lubridate", "skimr", "mvtnorm", "sf", "rstan", "loo"),
       library, character.only = TRUE
       )

rm(list = ls())
### source
source("source/20211001_Fct.r")

load("PTM_stanmodel_M1.RData.RData")
looM1 <- looic
load("PTM_stanmodel_M2.RData.RData")
looM2 <- looic
load("PTM_stanmodel_M3.RData.RData")
looM3 <- looic
load("PTM_stanmodel_M4.RData.RData")
looM4 <- looic
load("PTM_stanmodel_M5.RData.RData")
looM5 <- looic
load("PTM_stanmodel_M6.RData.RData")
looM6 <- looic
rm(ptm_model, looic)
### model selection
loo_compare(x = list(looM1, looM2, looM3, looM4, looM5, looM6))
#        elpd_diff se_diff
# model6     0.0       0.0
# model5  -240.1      26.7
# model4  -403.0      40.3
# model3  -602.9      41.7
# model2 -1618.7      67.6
# model1 -1786.9      66.3

### select M6
load("PTM_stanmodel_M6.RData.RData")
best_fit <- ptm_model; rm(ptm_model, looic, looM1, looM2, looM3, looM4, looM5, looM6)

plot(best_fit,
     plotfun = 'rhat',
     pars = c("intercept", "shape", "sigma", "epsilon", "alpha", "delta")
     )

traceplot(best_fit, 
          pars = c("intercept", "sigma", "shape"),
          inc_warmup = TRUE
          )

pairs(best_fit, pars = c("intercept", "sigma", "shape"))

print(best_fit, pars = c("intercept", "sigma", "rho", "shape"), dig = 3)

### cv of the gamma distribution
hist(1 / sqrt(rstan::extract(best_fit, "shape")$shape))

### Extract
n_zone <- best_fit@par_dims$w[3]
n_week <- best_fit@par_dims$w[1]
week <- 1:best_fit@par_dims$w[1]
year <- 1:best_fit@par_dims$w[2]
risk_profile <- duration_profile <- maree <- scalar <- NULL

time_to_run <- proc.time() # just to have an idea of the duration for the loop

### average
## risk
x <- theta(stanfit = best_fit, 
           week = 0, 
           year = 0, 
           response = "risk",
           param = "average"
           )
risk_profile <- rbind(risk_profile, 
                      data.frame(theta = apply(x, 2, mean),
                                 median = apply(x, 2, median),
                                 lower = apply(x, 2, lower),
                                 upper = apply(x, 2, upper),
                                 week = 1:n_week,
                                 year = rep(0, n_week),
                                 zone = rep(0, n_week),
                                 param = rep("mean", n_week)
                                 )
                      )
## duration
x <- theta(stanfit = best_fit, 
           week = 0, 
           year = 0, 
           response = "op_duration",
           param = "average"
           )
duration_profile <- rbind(duration_profile,
                          data.frame(theta = apply(x, 2, mean),
                                     median = apply(x, 2, median),
                                     lower = apply(x, 2, lower),
                                     upper = apply(x, 2, upper),
                                     week = 1:n_week,
                                     year = rep(0, n_week),
                                     zone = rep(0, n_week),
                                     param = rep("mean", n_week)
                                     )
                          )
## nb of operations
x <- theta(stanfit = best_fit, 
           week = 0, 
           year = 0, 
           response = "op_nb",
           param = "average"
           )
maree <- rbind(maree,
               data.frame(theta = apply(x, 2, mean),
                          median = apply(x, 2, median),
                          lower = apply(x, 2, lower),
                          upper = apply(x, 2, upper),
                          week = 1:n_week,
                          year = rep(0, n_week),
                          zone = rep(0, n_week),
                          param = rep("mean", n_week)
                          )
               )
for (i in week) {
  writeLines(paste("\tProcessing week", i, sep = " "))
  for (j in year) {
    writeLines(paste("\t\tProcessing year", j, sep = " "))
    ## risk
    # yearly-mean
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "risk",
               param = "average"
               )
    risk_profile <- rbind(risk_profile, 
                          data.frame(theta = apply(x, 2, mean),
                                     median = apply(x, 2, median),
                                     lower = apply(x, 2, lower),
                                     upper = apply(x, 2, upper),
                                     week = rep(i, n_zone),
                                     year = rep(j, n_zone),
                                     zone = rep(0, n_zone),
                                     param = rep("mean", n_zone)
                                     )
                          )
    # zone-specific deviation
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "risk",
               param = "deviation"
               )
    risk_profile <- rbind(risk_profile, 
                          data.frame(theta = apply(x, 2, mean),
                                     median = apply(x, 2, median),
                                     lower = apply(x, 2, lower),
                                     upper = apply(x, 2, upper),
                                     week = rep(i, n_zone),
                                     year = rep(j, n_zone),
                                     zone = 1:n_zone,
                                     param = rep("deviation", n_zone)
                                     )
                          )
    ## duration
    # yearly-mean
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "op_duration",
               param = "average"
               )
    duration_profile <- rbind(duration_profile,
                              data.frame(theta = apply(x, 2, mean),
                                         median = apply(x, 2, median),
                                         lower = apply(x, 2, lower),
                                         upper = apply(x, 2, upper),
                                         week = rep(i, n_zone),
                                         year = rep(j, n_zone),
                                         zone = rep(0, n_zone),
                                         param = rep("mean", n_zone)
                                         )
                              )
    # zone-specific deviation
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "op_duration",
               param = "deviation"
               )
    duration_profile <- rbind(duration_profile,
                              data.frame(theta = apply(x, 2, mean),
                                         median = apply(x, 2, median),
                                         lower = apply(x, 2, lower),
                                         upper = apply(x, 2, upper),
                                         week = rep(i, n_zone),
                                         year = rep(j, n_zone),
                                         zone = 1:n_zone,
                                         param = rep("deviation", n_zone)
                                         )
                              )
    ## nb of operations
    # yearly-mean
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "op_nb",
               param = "average"
               )
    maree <- rbind(maree,
                   data.frame(theta = apply(x, 2, mean),
                              median = apply(x, 2, median),
                              lower = apply(x, 2, lower),
                              upper = apply(x, 2, upper),
                              week = rep(i, n_zone),
                              year = rep(j, n_zone),
                              zone = rep(0, n_zone),
                              param = rep("mean", n_zone)
                              )
                   )
    # zone-specific deviation
    x <- theta(stanfit = best_fit, 
               week = i, 
               year = j, 
               response = "op_nb",
               param = "deviation"
               )
    maree <- rbind(maree,
                   data.frame(theta = apply(x, 2, mean),
                              median = apply(x, 2, median),
                              lower = apply(x, 2, lower),
                              upper = apply(x, 2, upper),
                              week = rep(i, n_zone),
                              year = rep(j, n_zone),
                              zone = 1:n_zone,
                              param = rep("deviation", n_zone)
                              )
                   )
    
    ## correction factor: average number of bycatch event per Days at Sea
    x <- rstan::extract(best_fit, 'w')$w[, i, j, ]
    scalar <- rbind(scalar, 
                    data.frame(theta = apply(x, 2, mean),
                               median = apply(x, 2, median),
                               lower = apply(x, 2, lower),
                               upper = apply(x, 2, upper),
                               week = rep(i, n_zone),
                               year = rep(j, n_zone),
                               zone = 1:n_zone,
                               param = rep("scalar", n_zone)
                               )
                    )
    
    rm(x)
    gc()
  }
}; rm(i, j)
gc()
writeLines(paste("The loop for risk profile took", round(((proc.time() - time_to_run)[3]) / 60, 2), "minutes to run"))
# The loop took 145 minutes to run

save(list = c("scalar", "maree", "duration_profile", "risk_profile"), 
     file = "output/ptm_synthetic_output.RData"
     )


