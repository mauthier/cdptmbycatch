##--------------------------------------------------------------------------------------------------------
## SCRIPT : Correlation matrix
##
## Authors : Matthieu Authier
## Last update : 2021-11-24
##
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "ggcorrplot"),
       library, character.only = TRUE
       )

rm(list = ls())
### source
source("source/20211001_Fct.r")

ggcorrplot(matlab::flipud(R), 
           ggtheme = theme_bw,
           method = "square", 
           type = "full", 
           outline.color = "#ffeda0"
           ) +
  scale_fill_viridis_c(name = "Correlation",
                       limits = c(0, 1)
                       ) +
  scale_x_continuous(breaks = c(0, 52), labels = c(1, 53)) +
  scale_y_continuous(breaks = c(0, 52), labels = c(53, 1)) +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_text(angle = 0),
        axis.title.y = element_blank(),
        panel.grid = element_blank()
        ) 
ggsave(filename = "figs/Omega.jpeg", 
       dpi = 600, units = 'cm',
       width = 10, height = 10
       )
